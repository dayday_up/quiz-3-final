package com.twuc.quiz.web;

import com.twuc.quiz.ApiTestBase;
import com.twuc.quiz.contract.RequestProduct;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MallControllerTest extends ApiTestBase {
    private ResultActions createProduct(RequestProduct product) throws Exception {
        return mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(product)));
    }

    private MvcResult createOrder() throws Exception {
        return mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
    }
    @Test
    void should_return_201_when_create_product() throws Exception {
        RequestProduct product = new RequestProduct("测试", "个", 1L, "/image.png");
        createProduct(product)
                .andExpect(status().isCreated());
    }

    @Test
    void should_return_product() throws Exception {
        RequestProduct product = new RequestProduct("测试", "个", 1L, "/image.png");
        createProduct(product);

        mockMvc.perform(get("/api/products/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value(product.getName()))
                .andExpect(jsonPath("$.unit").value(product.getUnit()))
                .andExpect(jsonPath("$.price").value(product.getPrice()))
                .andExpect(jsonPath("$.image").value(product.getImage()));
    }

    @Test
    void should_return_products() throws Exception {
        RequestProduct product = new RequestProduct("测试", "个", 1L, "/image.png");
        createProduct(product);
        createProduct(product);
        createProduct(product);

        mockMvc.perform(get("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    void should_return_200_when_add_product_to_order() throws Exception {
        RequestProduct product = new RequestProduct("测试", "个", 1L, "/image.png");
        createProduct(product);

        MvcResult order = createOrder();
        order.getResponse().getHeader("Location");

        mockMvc.perform(post("/api/orders/1/product/1"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_products_of_order() throws Exception {
        RequestProduct product = new RequestProduct("测试", "个", 1L, "/image.png");
        createProduct(product);

        MvcResult order = createOrder();
        order.getResponse().getHeader("Location");

        mockMvc.perform(get("/api/orders"))
                .andExpect(jsonPath("$", hasSize(1)));
    }
}
