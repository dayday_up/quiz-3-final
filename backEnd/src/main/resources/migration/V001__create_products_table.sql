CREATE TABLE products (
	id bigint AUTO_INCREMENT PRIMARY KEY,
	product_name varchar(128) NOT NULL,
	unit varchar(6) NOT NULL,
	price decimal(12, 2) NOT NULL,
	image varchar(256) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
