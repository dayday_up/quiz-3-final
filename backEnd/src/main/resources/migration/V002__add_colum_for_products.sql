ALTER TABLE products
    ADD COLUMN order_id bigint
        REFERENCES orders(id);
