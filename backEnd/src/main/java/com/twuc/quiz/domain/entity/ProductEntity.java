package com.twuc.quiz.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.twuc.quiz.contract.RequestProduct;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;

@Entity
@Table(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, name = "product_name")
    private String name;
    @Column(nullable = false)
    private String unit;
    @Column(nullable = false)
    private BigDecimal price;
    @Column(nullable = false)
    private String image;
    @Column
    private int count;

    @ManyToOne
    @JoinColumn(name = "order_id")
    @JsonIgnore
    private OrderEntity order;

    public ProductEntity() {
    }

    public ProductEntity(RequestProduct product) {
        this.name = product.getName();
        this.unit = product.getUnit();
        this.price = BigDecimal.valueOf(product.getPrice());
        this.image = product.getImage();
    }

    public OrderEntity getOrder() {
        return order;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getPrice() {
        return price;
    }



    public void setPrice(Float price) {
        this.price = BigDecimal.valueOf(price);
    }

    private String toDecimal(Float price) {
        DecimalFormat decimalFormat =new DecimalFormat(".00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
        return decimalFormat.format(price);
    }
}
