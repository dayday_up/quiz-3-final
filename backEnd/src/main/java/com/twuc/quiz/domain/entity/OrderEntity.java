package com.twuc.quiz.domain.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "order")
    private List<ProductEntity> products = new ArrayList<>();

    public OrderEntity() {
    }

    public List<ProductEntity> getProduct() {
        return products;
    }

    public void setProduct(ProductEntity product) {
        this.products.add(product);
    }
    public void removeProduct(ProductEntity product) {
        this.products.remove(product);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
