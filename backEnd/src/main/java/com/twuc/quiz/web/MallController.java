package com.twuc.quiz.web;

import com.twuc.quiz.contract.RequestProduct;
import com.twuc.quiz.domain.entity.OrderEntity;
import com.twuc.quiz.domain.entity.ProductEntity;
import com.twuc.quiz.domain.entityRepository.OrderRepository;
import com.twuc.quiz.domain.entityRepository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class MallController {
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;

    public MallController(ProductRepository productRepository, OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    @PostMapping("/products")
    ResponseEntity createProducts(@RequestBody RequestProduct product) {
        productRepository.save(product.toEntity());
        return ResponseEntity.status(HttpStatus.CREATED)
                .build();
    }

    @GetMapping("/products/{productId}")
    ResponseEntity findProductById(@PathVariable Long productId) {
        Optional<ProductEntity> findProduct = productRepository.findById(productId);
        return generateResponseEntity(HttpStatus.OK, MediaType.APPLICATION_JSON_UTF8)
                .body(findProduct);
    }

    @GetMapping("/products")
    ResponseEntity findAllProduct() {
        List<ProductEntity> products = productRepository.findAll();
        return generateResponseEntity(HttpStatus.OK, MediaType.APPLICATION_JSON_UTF8)
                .body(products);
    }

    @PostMapping("/orders")
    ResponseEntity createOrder() {
        orderRepository.save(new OrderEntity());
        return ResponseEntity.status(HttpStatus.CREATED)
                .build();
    }

    @PostMapping("/orders/{orderId}/product/{productId}")
    ResponseEntity addProductToOrder(@PathVariable Long orderId, @PathVariable Long productId) {
        Optional<ProductEntity> product = productRepository.findById(productId);
        Optional<OrderEntity> order = orderRepository.findById(orderId);
        OrderEntity orderEntity = order.get();
        ProductEntity productEntity = product.get();

        if(null == productEntity.getOrder()) {
            productEntity.setCount(1);
        } else {
            productEntity.setCount(productEntity.getCount() + 1);
            productEntity.setOrder(orderEntity);
        }

        productEntity.setOrder(orderEntity);
        ProductEntity savedProduct = productRepository.save(productEntity);


        return ResponseEntity.status(HttpStatus.OK)
                .header("Location", "/orders/" + order.get().getId())
                .body("");
    }

    @GetMapping("/orders")
    ResponseEntity findAllProductOfOrder() {
        List<OrderEntity> products = orderRepository.findAll();
        return generateResponseEntity(HttpStatus.OK, MediaType.APPLICATION_JSON_UTF8)
                .body(products);
    }



    public ResponseEntity.BodyBuilder generateResponseEntity(HttpStatus status, MediaType mediaType) {
        return ResponseEntity.status(status)
                .contentType(MediaType.APPLICATION_JSON_UTF8);
    }
}
