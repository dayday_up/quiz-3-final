package com.twuc.quiz.contract;

import com.twuc.quiz.domain.entity.ProductEntity;

import javax.validation.constraints.NotNull;

public class RequestProduct {
    @NotNull
    private String name;
    @NotNull
    private String unit;
    @NotNull
    private float price;
    @NotNull
    private String image;

    public RequestProduct(String name, String unit, float price, String image) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ProductEntity toEntity() {
        return new ProductEntity(this);
    }
}
