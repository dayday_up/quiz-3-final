import * as React from "react";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from 'react-router';
import Header from './components/Header';
import AddProduct from './container/AddProduct';
import ProductList from './container/ProductList';
import OrderList from './container/OrderList';

class App extends React.Component {

  render() {
    return (
      <div className="app">
        <Router>
          <Header/>
          <div className="pages">
            <Switch>
              <Route path="/" exact component={ProductList}/>
              <Route path="/add" exact component={AddProduct}/>
              <Route path="/order" exact component={OrderList}/>
            </Switch>
          </div>
        </Router>
      </div>
    )
  }
}

export default App;
