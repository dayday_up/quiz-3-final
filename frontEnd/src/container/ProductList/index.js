import React, {Component} from 'react';
import {get, post} from '../../common/js/request';
import './style'

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    }
  }

  componentDidMount() {
    get("/api/products")
      .then(res => {
        this.setState({
          products: res
        })
      })
  }

  handleBuy = (productId) => {
    return () => {
      post(`/api/orders/1/product/${productId}`)
        .then(() => {
          console.log("aaa")
        })
    }
  };

  render() {
    const { products } = this.state;

    return (
      <div className="product-list">
        <ul className="products">
          {products.map(item => {
            return(
              <li className="products-item" key={item.id}>
                <img className="image" src={item.image} alt={item.name}/>
                <h2 className="name">{item.name}</h2>
                <p className="price">{`${item.price}元/${item.unit}`}</p>
                <span className="buy" onClick={this.handleBuy(item.id)}>+</span>
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

export default ProductList;
