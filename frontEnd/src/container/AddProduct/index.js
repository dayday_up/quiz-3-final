import React, {Component} from 'react';
import {post} from '../../common/js/request';
import './style.less';

class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      price: "",
      unit: "",
      image: ""
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    post("/api/products", this.state)
      .then(res => {
        console.log(res);
      })
  };

  handleNameChange = (event) => {
    const name = event.target.value;
    this.setState({
      name: name
    })
  };
  handlePriceChange = (event) => {
    const price = event.target.value;
    this.setState({
      price: price
    })
  };handleUnitChange = (event) => {
    const unit = event.target.value;
    this.setState({
      unit: unit
    })
  };
  handleImageChange = (event) => {
    const image = event.target.value;
    this.setState({
      image: image
    })
  };


  render() {
    const { name, price, unit, image } = this.state;
    return (
      <div className="product">
        <h1 className="title">添加商品</h1>
        <form className="product-form" onSubmit={this.handleSubmit}>
          <div className="field">
            <label htmlFor="name"><span className="required">*</span>名称</label>
            <input type="text" id="name" value={name} onChange={this.handleNameChange} className="name"/>
          </div>
          <div className="field">
            <label htmlFor="price"><span className="required">*</span>价格</label>
            <input type="text" id="price" value={price} onChange={this.handlePriceChange} className="price"/>
          </div>
          <div className="field">
            <label htmlFor="unit"><span className="required">*</span>单位</label>
            <input type="text" id="unit" value={unit} onChange={this.handleUnitChange} className="unit"/>
          </div>
          <div className="field">
            <label htmlFor="image"><span className="required">*</span>图片</label>
            <input type="text" id="image" value={image} onChange={this.handleImageChange} className="image"/>
          </div>
          <div className="button-wrapper">
            <button className="submit" type="submit">提交</button>
          </div>
        </form>
      </div>
    );
  }
}

export default AddProduct;
