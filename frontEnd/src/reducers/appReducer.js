const initState = {
  name: ''
};

export default (state = initState, action = {})=>{
  switch (action.type) {
    case "INIT":
      return {
        ...state,
        name: action.data
      };
    default:
      return {
        ...state
      };
  }
}