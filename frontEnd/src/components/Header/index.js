import {Link} from 'react-router-dom';
import React from 'react';
import './style.less';

const Header = (props) => {
    return (
      <div>
        <nav className="nav-bar">
          <ul className="nav">
            <li className="link">
              <Link to="/">商城</Link>
            </li>
            <li className="link">
              <Link to="/order">订单</Link>
            </li>
            <li className="link">
              <Link to="/add">添加商品</Link>
            </li>
          </ul>
        </nav>
      </div>
    );
};

export default Header;
